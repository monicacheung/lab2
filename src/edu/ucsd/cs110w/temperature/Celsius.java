/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wai
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return this.getValue()+" C";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius(this.getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		return new Celsius((float)(this.getValue()*1.8000+32.00));
	}
	@Override 
	public Temperature toKelvin() {
		return new Celsius( (float) (this.getValue()+273.15));
	}

}
