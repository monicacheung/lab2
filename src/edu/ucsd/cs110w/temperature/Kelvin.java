/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110wai): write class javadoc
 *
 * @author cs110wai
 *
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) {
		super(t);
	}
	
	public String toString()
	{
		return this.getValue()+" K";
	}
	
	@Override
	public Temperature toCelsius() {
		return new Kelvin((float)(this.getValue()-273.15));
	}
	@Override
	public Temperature toFahrenheit() {
		return new Kelvin((float)( (this.getValue()-273.15) *1.8000+32.00));
	}

	@Override
	public Temperature toKelvin() {
		return new Kelvin(this.getValue());
	}

}
