/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wai
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return this.getValue()+" F";
	}
	@Override
	public Temperature toCelsius() {
		return new Fahrenheit((float)((this.getValue()-32)/1.800));
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit(this.getValue());
	}
	public Temperature toKelvin() {
		return new Fahrenheit((float) (((this.getValue()-32)/1.8)+273.15));
	}

}
