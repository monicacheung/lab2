package edu.ucsd.cs110w.tests;


import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Temperature;
import junit.framework.TestCase;


public class FahrenheitTests extends TestCase {
	private float delta = 0.001f;
	public void testFahrenheit() {
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testFahrenheitToString() {
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " F";
		assertTrue(string.startsWith(beginning));
		assertTrue(string.endsWith(ending));
		int endIndex = string.indexOf(ending);
		assertTrue(string.substring(0, endIndex).equals(beginning));
	}
	
	public void testFahrenheitToFahrenheit() {		
		Fahrenheit temp = new Fahrenheit(0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(0, convert.getValue(), delta);
	}
	
	public void testFahrenheitToCelsius(){
		Fahrenheit temp = new Fahrenheit(0);
		Temperature convert = temp.toCelsius();
		assertEquals(-17.7778, convert.getValue(), delta);
		
	}
	
	public void testFahrenheitToKelvin(){
		Fahrenheit temp = new Fahrenheit(0);
		Temperature convert = temp.toKelvin();
		assertEquals(255.372, convert.getValue(), delta);

	}
}
