package edu.ucsd.cs110w.tests;

import edu.ucsd.cs110w.temperature.Kelvin;
import edu.ucsd.cs110w.temperature.Temperature;
import junit.framework.TestCase;

public class KelvinTests extends TestCase {
	private float delta = 0.001f;
	public void testCelsius(){
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testKelvinToString(){
		float value = 12.34f;
		Kelvin temp = new Kelvin(value);
		String string = temp.toString();
		String beginning = "" + value;
		String ending = " K";
		assertTrue(string.startsWith(beginning));
		assertTrue(string.endsWith(ending));
		int endIndex = string.indexOf(ending);
		assertTrue(string.substring(0, endIndex).equals(beginning));
		
	}
	public void testKelvinToKelvin(){
		Kelvin temp = new Kelvin(0);
		Temperature convert = temp.toKelvin();
		assertEquals(0, convert.getValue(), delta);
	}
	public void testKelvinToCelsius(){
		Kelvin temp = new Kelvin(0);
		Temperature convert = temp.toCelsius();
		assertEquals(-273.15, convert.getValue(), delta);
		
		temp = new Kelvin(100);
		convert = temp.toCelsius();
		assertEquals(-173.15, convert.getValue(), delta);
	}
	public void testKelvinToFahrenheit(){
		Kelvin temp = new Kelvin(0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(-459.67, convert.getValue(), delta);
		temp = new Kelvin(100);
		convert = temp.toFahrenheit();
		
		assertEquals(-279.67, convert.getValue(), delta);
		
	}

}
